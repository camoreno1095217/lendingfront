import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def home():
    return "<h1>API is Running</h1>"

@app.route('/api/v1/resources/loan/validate', methods=['POST'])
def validate_loan():
    if 'amount' in request.args:
        amount = int(request.args['amount'])
    else:
        return "Error: No amount field provided. Please specify an amount."

    if amount > 50000:
        resultado = {'result':'Declined'}
    elif amount == 50000:
        resultado = {'result':'Undecided'}
    else:
        resultado = {'result':'Approved'}

    return jsonify(resultado)

app.run()