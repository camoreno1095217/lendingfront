import React, { Component } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';

export class EmptyPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            labelLoan: this.props.labelLoan
        };
        this.requestLoan = this.requestLoan.bind(this);
    }

    componentDidMount() {
    }

    requestLoan(event) {
        var amount = document.getElementById("txtAmount").value;
        fetch("http://localhost:5000/api/v1/resources/loan/validate?amount=" + amount,
            {
                method: 'POST',
                headers: new Headers(
                    {
                        "Content-Type": "application/json"
                    }
                )
            })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        visible: true,
                        labelLoan: result.result
                    });
                },
                (error) => {
                    console.log(error);
                    this.setState({
                        visible: false
                    });
                }
            )
    }

    render() {
        return (
            <form onSubmit={this.requestLoan}>
                <div className="p-grid p-fluid">
                    <div className="p-col-12">
                        <div className="card">
                            <h1>Business</h1>
                            <div className="p-grid">
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Tax Id" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Business Name" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="City" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="State" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Postal Code" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText id="txtAmount" placeholder="Requested amount" required="true" keyfilter="pint" />
                                </div>
                            </div>
                        </div>

                        <div className="card">
                            <h1>Owner</h1>
                            <div className="p-grid">
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Social Security Number" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Name" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Email" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Address" required="true" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="City" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="State" />
                                </div>
                                <div className="p-col-12 p-md-12">
                                    <InputText placeholder="Postal Code" />
                                </div>
                            </div>
                        </div>

                        <Button label="Request" type="submit" onClick={() => this.setState({ showModal: true })} />

                        <Dialog id="dlg" header="Loan Decision" visible={this.state.visible} modal={true} width="400px" onHide={() => this.setState({ visible: false })}>
                            <p>Your loan has been: {this.state.labelLoan}</p>
                        </Dialog>

                    </div>
                </div>
            </form>


        );
    }
}