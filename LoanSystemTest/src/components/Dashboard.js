import React, { Component } from 'react';
import {Panel} from 'primereact/panel';
import {Chart} from 'primereact/chart';

export class Dashboard extends Component {

    constructor() {
        super();
        this.state = {
            lineData: {
                labels: ['February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                    {
                        label: 'Payments',
                        data: [900, 1000, 900, 900, 900, 1000],
                        fill: false,
                        borderColor: '#007be5'
                    }
                ]
            }
        };
    }

    componentDidMount() {
    }

    render() {        
        return (
            <div className="p-grid p-fluid dashboard">
                <div className="p-col-12 p-lg-4">
                    <div className="card summary">
                        <span className="title">Remain</span>
                        <span className="detail">Remain Balance</span>
                        <span className="count visitors">$3,400</span>
                    </div>
                </div>
                <div className="p-col-12 p-lg-4">
                    <div className="card summary">
                        <span className="title">Paid</span>
                        <span className="detail">Paid out</span>
                        <span className="count purchases">$5,600</span>
                    </div>
                </div>
                <div className="p-col-12 p-lg-4">
                    <div className="card summary">
                        <span className="title">Pending</span>
                        <span className="detail">Pending Payments</span>
                        <span className="count revenue">4</span>
                    </div>
                </div>

                <div className="p-col-12 p-lg-8">
                    <div className="card">
                        <Chart type="line" data={this.state.lineData}/>
                    </div>
                </div>

                <div className="p-col-12 p-lg-4">
                    <Panel header="Payment Activity" style={{height:'100%'}}>
                        <div className="activity-header">
                            <div className="p-grid">
                                <div className="p-col-12">
                                    <p>Last activity that you made in your loan</p>
                                </div>
                            </div>
                        </div>

                        <ul className="activity-list">
                            <li>
                                <div className="count">$900</div>
                                <div className="p-grid">
                                    <div className="p-col-6">February</div>
                                </div>
                            </li>
                            <li>
                                <div className="count" style={{backgroundColor:'#20d077'}}>$1,000</div>
                                <div className="p-grid">
                                    <div className="p-col-6">March</div>
                                </div>
                            </li>
                            <li>
                                <div className="count">$900</div>
                                <div className="p-grid">
                                    <div className="p-col-6">April</div>
                                </div>
                            </li>
                            <li>
                                <div className="count">$900</div>
                                <div className="p-grid">
                                    <div className="p-col-6">May</div>
                                </div>
                            </li>
                            <li>
                                <div className="count">$900</div>
                                <div className="p-grid">
                                    <div className="p-col-6">June</div>
                                </div>
                            </li>
                            <li>
                                <div className="count" style={{backgroundColor:'#20d077'}}>$1,000</div>
                                <div className="p-grid">
                                    <div className="p-col-6">July</div>
                                </div>
                            </li>
                        </ul>
                    </Panel>
                </div>
            </div>
        );
    }
}